#include <fstream>
#include <vector>

std::vector<float> *open_pcm_float(std::string filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    std::vector<float> *data = new std::vector<float>();
    file.seekg(0, std::ios::end);
    const size_t elements = file.tellg() / sizeof(float);
    data->resize(elements);
    file.seekg(0, std::ios::beg);
    file.read(reinterpret_cast<char *>(data->data()), elements * sizeof(float));

    return data;
}
