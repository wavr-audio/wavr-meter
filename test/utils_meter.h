#include <algorithm>
#include <chrono>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <iostream>

#include <gtkmm.h>

#include "utils.h"
#include <wavr/wavr-meter.h>

#define SAMPLE_RATE 48000
#define BUFFER_SIZE 2048
#define CHANNELS 2

using namespace Wavr;

bool do_update_meters(const std::chrono::time_point<std::chrono::steady_clock> &start, Meter *meter, Gtk::Label *label,
                      std::vector<float> *data) {
    auto end = std::chrono::steady_clock::now();
    double elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();
    size_t sample = (SAMPLE_RATE * CHANNELS) * elapsed;
    std::ostringstream txt;
#if ENABLE_TIMER
    txt << (10.0 - elapsed) << " s";
#else
    txt << elapsed << " s";
#endif

    meter->process(BUFFER_SIZE, CHANNELS, data->data() + sample);
    label->set_text(txt.str());
#if 0
    std::cout << std::setw(10) << elapsed << "s";
    std::cout << std::endl;
#endif
    return true;
}
int run_meter_type(Meter::ProcessingMode processing_mode) {
    auto app = Gtk::Application::create("com.gitlab.Wavr.TestMeter");
    std::vector<float> *track = open_pcm_float("track.pcm");
    auto meter = new Meter(SAMPLE_RATE, CHANNELS);
    meter->set_processing_mode(processing_mode);
    Gtk::Window win;
    Gtk::Box box(Gtk::Orientation::ORIENTATION_VERTICAL);
    auto label = new Gtk::Label("10 s");
    win.set_border_width(5);
    box.pack_start(*meter);
    box.pack_start(*label, Gtk::PackOptions::PACK_SHRINK);
    win.add(box);
    g_object_set(gtk_settings_get_default(), "gtk-application-prefer-dark-theme", true, NULL);

    std::chrono::time_point<std::chrono::steady_clock> start;
    start = std::chrono::steady_clock::now();
    auto slot = sigc::bind(sigc::ptr_fun(&do_update_meters), start, meter, label, track);
    auto conn = Glib::signal_timeout().connect(slot, 16);
#if ENABLE_TIMER
    auto conn_quit = Glib::signal_timeout().connect(
        [&]() {
            app->quit();
            return false;
        },
        10e3);
#endif
    meter->set_db_limits(-20, 2);
    meter->set_falloff_speed(2.f);
    box.show_all();
    auto retval = app->run(win);
    conn.disconnect();
    return retval;
}
