#include <wavr/meter/meter.h>

#include "monitors/monitor_ebu_momentary.h"
#include "monitors/monitor_ebu_short.h"
#include "monitors/monitor_ebu_tpm.h"
#include "monitors/monitor_ppm.h"
#include "monitors/monitor_rms.h"

using namespace Wavr;

inline float clamp(const float &n) { return std::max(0.0f, std::min(n, 1.0f)); }

Meter::Meter(size_t samplerate, size_t channels)
    : Glib::ObjectBase("wavr-meter"), Gtk::DrawingArea(), m_level(channels), m_prevlvl(channels), m_convert_db(true),
      m_samplerate(samplerate), m_dbmin(-48), m_dbmax(6), m_inmargin(6.f), m_showticks(true), m_falloff_speed(3.0f) {
    set_has_window(true);
    set_name("wavr-meter");

    std::fill(m_level.begin(), m_level.end(), 0.0);

    m_dispatch_redraw.connect(sigc::mem_fun(*this, &Meter::invalidate));
    m_start = std::chrono::steady_clock::now();

    m_monitor = new PPMMonitor(m_samplerate);
}

void Meter::set_levels(std::vector<float> levels) {
    assert(levels.size() == m_level.size());
    std::copy(levels.begin(), levels.end(), m_level.begin());
    invalidate_safe();
}

void Meter::set_processing_mode(ProcessingMode mode) {
    delete m_monitor;
    switch (mode) {
    case PROCESSING_TPM:
        m_monitor = new EBUTruePeakMonitor(m_samplerate);
        break;
    case PROCESSING_RMS:
        m_monitor = new RMSMonitor(m_samplerate);
        break;
    case PROCESSING_EBU_SHORT:
        m_monitor = new EBUShortMonitor(m_samplerate);
        break;
    case PROCESSING_EBU_MOMENTARY:
        m_monitor = new EBUMomentaryMonitor(m_samplerate);
        break;
    case PROCESSING_PPM:
    default:
        m_monitor = new PPMMonitor(m_samplerate);
        break;
    }
}

void Meter::process(size_t frame_size, unsigned short channels, float *data) {
    m_level = m_monitor->process(frame_size, channels, data);
    invalidate_safe();
}
void Meter::process(const std::vector<std::vector<float>> &data) {
    m_level = m_monitor->process(data);
    invalidate_safe();
}
void Meter::set_db_limits(float min, float max) {
    m_dbmin = (min);
    m_dbmax = (max);
    invalidate_safe();
}

void Meter::get_preferred_width_for_height_vfunc(int, int &min, int &natural) const {
    get_preferred_width_vfunc(min, natural);
}
void Meter::get_preferred_width_vfunc(int &min, int &natural) const {
    min = 20 * m_level.size() + TICKS_SIDE_WIDTH;
    natural = 50 * m_level.size();
}
void Meter::get_preferred_height_for_width_vfunc(int, int &min, int &natural) const {
    get_preferred_height_vfunc(min, natural);
}
void Meter::get_preferred_height_vfunc(int &min, int &natural) const {
    min = 100;
    natural = 400;
}

bool Meter::on_draw(const Cairo::RefPtr<Cairo::Context> &ctx) {
    auto now = std::chrono::steady_clock::now();
    std::chrono::duration<float> dt = now - m_start;
    auto alloc = get_allocation();
    auto ref_style_ctx = get_style_context();
    float width = alloc.get_width();
    float height = alloc.get_height();
    float single_width_outer = (width - TICKS_SIDE_WIDTH) / m_level.size();
    float single_width = single_width_outer - m_inmargin;
    int num_leds = height / METERLED_TARGET_SIZE;

    ref_style_ctx->render_background(ctx, alloc.get_x(), alloc.get_y(), width, height);
    ctx->translate(alloc.get_x(), alloc.get_y());

    for (size_t i = 0; i < m_level.size(); i++) {
        // TODO: Fix falloff issue
        float falloff = std::exp(-m_falloff_speed * dt.count());
        float v = m_level[i];
        if (m_monitor->is_output_linear()) {
            v = std::max(v, falloff * m_prevlvl[i]);
            m_prevlvl[i] = v;
            v = linear_to_db(v);
        } else {
            if (std::abs(m_prevlvl[i]) < 0.01)
                m_prevlvl[i] = -INFINITY;
            v = std::max(v, m_prevlvl[i] - 5.f * m_falloff_speed * dt.count());
            m_prevlvl[i] = v;
        }
        if (m_convert_db)
            v = map_db(v);
        ctx->save();
        ctx->set_source_rgba(0.0, 0.0, 0.0, 0.1);
        ctx->rectangle(i * single_width_outer, 0, single_width, height);
        ctx->fill();
        ctx->restore();

        for (int j = 0; j < num_leds; j++) {
            float ypos = j / (float)num_leds;
            if (v > ypos) {
                if (ypos >= map_db(0.f))
                    ctx->set_source_rgb(1.0, 0.0, 0.0);
                else if (ypos > map_db(-6.f))
                    ctx->set_source_rgb(1.0, 1.0, 0.0);
                else
                    ctx->set_source_rgb(0.1, 0.8, 0.2);
                ctx->rectangle(i * single_width_outer, (1.f - ypos) * (height), single_width,
                               height / (float)num_leds - 1.f);
                ctx->fill();
            }
        }
    }

    if (!m_showticks) {
        m_start = std::chrono::steady_clock::now();
        return true;
    }
    // Draw ticks
    ctx->save();
    ctx->set_source_rgb(0.5, 0.5, 0.5);
    ctx->set_line_width(0.2);
    for (int i = m_dbmax; i > m_dbmin; i--) {
        ctx->save();
        ctx->translate(width - TICKS_SIDE_WIDTH, height * (1.f - map_db(i)));
        ctx->move_to(0, 0);
        ctx->line_to(2, 0);
        ctx->restore();
    }
    ctx->stroke();
    ctx->set_source_rgb(1.0, 1.0, 1.0);

    for (int i = 0; i < m_dbmax; i += 6) {
        draw_tick(ctx, alloc, i);
    }
    for (int i = -6; i > m_dbmin; i -= 6) {
        draw_tick(ctx, alloc, i);
    }
    ctx->restore();

    m_start = std::chrono::steady_clock::now();
    return true;
}

void Meter::invalidate() {
    queue_draw();
#if 0
    std::cout << "Meter::invalidate" << std::endl;
#endif
}
void Meter::invalidate_safe() { m_dispatch_redraw.emit(); }

void Meter::draw_tick(const Cairo::RefPtr<Cairo::Context> &ctx, Gtk::Allocation &alloc, int db) const {
    ctx->save();
    ctx->translate(alloc.get_width() - TICKS_SIDE_WIDTH, alloc.get_height() * (1.f - map_db((float)db)));
    ctx->move_to(0, 0);
    ctx->line_to(3, 0);
    ctx->set_line_width(0.5);
    ctx->stroke();
    ctx->move_to(5, 2.5);
    ctx->set_font_size(8);
    ctx->show_text(std::to_string(db));
    ctx->fill();
    ctx->restore();
}

float Meter::linear_to_db(float v) const { return 20.0 * std::log10(v); }
float Meter::map_db(float db) const { return clamp((db - m_dbmin) / (float)(m_dbmax - m_dbmin)); }
