#pragma once
#include <vector>

#include <ebur128.h>

#include "monitor_ebu_base.h"

namespace Wavr {
class EBUShortMonitor : public EBUBaseMonitor {
public:
    EBUShortMonitor(size_t samplerate) : EBUBaseMonitor(samplerate, EBUR128_MODE_S) {}

    double process_ebu(ebur128_state *state, size_t frame_size, const float *data) override;
};
} // namespace Wavr
