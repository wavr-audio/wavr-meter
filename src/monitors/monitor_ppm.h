#pragma once
#include <algorithm>
#include <vector>

#include <ebur128.h>

#include <wavr/meter/monitor.h>

namespace Wavr {
class PPMMonitor : public Monitor {
public:
    PPMMonitor(size_t samplerate) : Monitor() { m_state = ebur128_init(1, samplerate, EBUR128_MODE_TRUE_PEAK); };
    dlevels_t process(const data_t &) override;

private:
    ebur128_state *m_state;
};
} // namespace Wavr
