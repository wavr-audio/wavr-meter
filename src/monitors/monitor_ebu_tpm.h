#pragma once
#include <vector>

#include <ebur128.h>

#include "monitor_ebu_base.h"

namespace Wavr {
class EBUTruePeakMonitor : public EBUBaseMonitor {
public:
    EBUTruePeakMonitor(size_t samplerate) : EBUBaseMonitor(samplerate, EBUR128_MODE_TRUE_PEAK) {}

    double process_ebu(ebur128_state *state, size_t frame_size, const float *data) override;
};
} // namespace Wavr
