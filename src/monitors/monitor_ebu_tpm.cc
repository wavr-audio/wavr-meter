#include "monitors/monitor_ebu_tpm.h"

namespace Wavr {
double EBUTruePeakMonitor::process_ebu(ebur128_state *state, size_t frame_size, const float *data) {
    double level = -100.0;
    ebur128_add_frames_float(state, data, frame_size);
    ebur128_prev_true_peak(state, 0, &level);
    return level;
}
} // namespace Wavr
