#include "monitor_rms.h"

using namespace Wavr;

RMSMonitor::dlevels_t RMSMonitor::process(const data_t &data) {
    dlevels_t rms(data.size());
    dlevels_t levels(data.size());
    if (m_framesize != data[0].size()) {
        m_rmsdata.clear();
        m_framesize = data[0].size();
        set_rmslength(m_rmslength);
    }
    m_rmslevels.resize(data.size());

    for (size_t i = 0; i < rms.size(); i++) {
        rms[i] = 0.f;
        for (size_t s = 0; s < data[i].size(); s++) {
            rms[i] += data[i][s] * data[i][s];
        }
        rms[i] = std::sqrt(rms[i] / data[i].size());
    }
    if (m_rmsdata.size() < m_rmslength * (m_samplerate / m_framesize)) {
        if (m_rmsdata.empty()) {
            m_rmsdata.reserve(m_rmslength * (m_samplerate / m_framesize));
            m_rmslevels = rms;
        }
        m_rmsdata.push_back(rms);
        for (size_t i = 0; i < rms.size(); i++) {
            m_rmslevels[i] += rms[i] / (m_rmslength * (m_samplerate / m_framesize));
        }
    } else {
        for (size_t i = 0; i < m_rmslevels.size(); i++) {
            m_rmslevels[i] += m_framesize * (rms[i] - m_rmsdata[0][i]) / (m_rmslength * (m_samplerate));
        }
        m_rmsdata.erase(m_rmsdata.begin());
        m_rmsdata.push_back(rms);
    }

    return m_rmslevels;
}

void RMSMonitor::set_rmslength(const double length) {
    if (length == m_rmslength)
        return;
    if (length > m_rmslength) {
    } else {
        m_rmsdata.reserve(length * (m_samplerate / m_framesize));
    }
    m_rmslength = length;
}

void RMSMonitor::set_rmslength(const double length, const size_t frame_size) {
    m_framesize = frame_size;
    set_rmslength(length);
}
