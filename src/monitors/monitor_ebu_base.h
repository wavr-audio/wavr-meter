#pragma once
#include <vector>

#include <ebur128.h>

#include <wavr/meter/monitor.h>

namespace Wavr {
class EBUBaseMonitor : public Monitor {
public:
    EBUBaseMonitor(size_t samplerate, int mode) : Monitor(), m_states(1), m_samplerate(samplerate) {
        m_states[0] = ebur128_init(1, samplerate, mode);
    }
    virtual ~EBUBaseMonitor() { m_states.clear(); };

    dlevels_t process(const data_t &data) override;
    virtual double process_ebu(ebur128_state *state, size_t frame_size, const float *data){};
    size_t get_samplerate() { return m_samplerate; }
    bool is_output_linear() override { return false; }

private:
    std::vector<ebur128_state *> m_states;
    size_t m_samplerate;
};
} // namespace Wavr
