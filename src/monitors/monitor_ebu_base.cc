#include "monitor_ebu_base.h"

using namespace Wavr;

EBUBaseMonitor::dlevels_t EBUBaseMonitor::process(const data_t &data) {
    dlevels_t levels(data.size());
    if (m_states.size() != data.size()) {
        m_states.resize(data.size());
        for (size_t i = 0; i < m_states.size(); i++) {
            if (m_states[i]) {
                ebur128_change_parameters(m_states[i], 1, m_states[i]->samplerate);
            } else {
                m_states[i] = ebur128_init(1, m_samplerate, m_states[0]->mode);
            }
        }
    }
    for (size_t i = 0; i < data.size(); i++) {
        double level = process_ebu(m_states[i], data[i].size(), data[i].data());
        levels[i] = level;
    }

    return levels;
}
