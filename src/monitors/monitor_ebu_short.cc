#include "monitor_ebu_short.h"

using namespace Wavr;

double EBUShortMonitor::process_ebu(ebur128_state *state, size_t frame_size, const float *data) {
    double level = 100.0;
    ebur128_add_frames_float(state, data, frame_size);
    ebur128_loudness_shortterm(state, &level);
    return level;
}
