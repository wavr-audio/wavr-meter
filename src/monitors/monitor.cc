#include <wavr/meter/monitor.h>

using namespace Wavr;

Monitor::dlevels_t Monitor::process(const size_t frame_size, const unsigned short channels, const float *data) {
    size_t deinterleave_idx = 0;
    data_t vec_data(channels);
    std::for_each(data, data + frame_size * channels, [&](const float &item) {
        vec_data[deinterleave_idx].push_back(item);
        deinterleave_idx = (deinterleave_idx + 1) % channels;
    });
    return process(vec_data);
}
