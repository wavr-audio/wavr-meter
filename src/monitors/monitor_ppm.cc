#include "monitor_ppm.h"

using namespace Wavr;

PPMMonitor::dlevels_t PPMMonitor::process(const data_t &data) {
    dlevels_t levels(data.size());
#if 0
    for (size_t i = 0; i < levels.size(); i++) {
        levels[i] = *std::max_element(data[i].begin(), data[i].end());
    }
#else
    std::vector<float> buffer(data.size() * data[0].size());
    if (m_state->channels != data.size()) {
        ebur128_change_parameters(m_state, data.size(), m_state->samplerate);
    }
    for (size_t c = 0; c < data.size(); c++) {
        for (size_t i = 0; i < data[c].size(); i++) {
            buffer.at(i * data.size() + c) = data[c][i];
        }
    }
    ebur128_add_frames_float(m_state, buffer.data(), data[0].size());
    for (size_t i = 0; i < m_state->channels; i++) {
        double peak;
        ebur128_prev_true_peak(m_state, i, &peak);
        levels[i] = peak;
    }
#endif
    return levels;
}
