#pragma once
#include <algorithm>
#include <cmath>
#include <vector>

#include <wavr/meter/monitor.h>

namespace Wavr {
class RMSMonitor : public Monitor {
public:
    RMSMonitor(size_t samplerate) : Monitor(), m_samplerate(samplerate), m_rmslength(1.0), m_rmsdata() {}
    dlevels_t process(const data_t &data) override;
    void set_rmslength(const double length);
    void set_rmslength(const double length, const size_t frame_size);

private:
    size_t m_samplerate;
    size_t m_framesize;
    double m_rmslength;
    std::vector<dlevels_t> m_rmsdata;
    dlevels_t m_rmslevels;
};
} // namespace Wavr
