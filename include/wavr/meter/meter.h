#pragma once
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#include <gdkmm/general.h>
#include <glibmm/dispatcher.h>
#include <gtkmm/drawingarea.h>

#include "monitor.h"

#define METERLED_TARGET_SIZE 5
#define TICKS_SIDE_WIDTH 25.f

namespace Wavr {
class Meter : public Gtk::DrawingArea {
public:
    enum ProcessingMode {
        PROCESSING_PPM,
        PROCESSING_TPM,
        PROCESSING_RMS,
        PROCESSING_EBU_SHORT,
        PROCESSING_EBU_MOMENTARY,
        PROCESSING__MAX = PROCESSING_EBU_MOMENTARY
    };

    Meter(size_t samplerate, size_t channels);
    virtual ~Meter() { delete m_monitor; }
    virtual void process(size_t frame_size, unsigned short channels, float *data);
    virtual void process(const std::vector<std::vector<float>> &data);
    void set_levels(std::vector<float> levels);
    void set_convert_decibel(bool do_convert) { m_convert_db = do_convert; }
    void set_db_limits(float min, float max);
    bool is_decibel() { return m_convert_db; }
    size_t get_channel_count() { return m_level.size(); }
    void set_show_ticks(bool show = true) { m_showticks = show; }
    void set_inner_margin(float margin) { m_inmargin = std::max(2.f, margin); }
    void set_falloff_speed(float speed) { m_falloff_speed = std::max(0.001f, speed); }
    void set_processing_mode(ProcessingMode mode);
    Monitor *get_monitor() { return m_monitor; }

protected:
    void get_preferred_width_vfunc(int &minimum_width, int &natural_width) const override;
    void get_preferred_height_for_width_vfunc(int width, int &minimum_height, int &natural_height) const override;
    void get_preferred_height_vfunc(int &minimum_height, int &natural_height) const override;
    void get_preferred_width_for_height_vfunc(int height, int &minimum_width, int &natural_width) const override;
    virtual bool on_draw(const Cairo::RefPtr<Cairo::Context> &ctx) override;
    void invalidate();
    void invalidate_safe();
    void draw_tick(const Cairo::RefPtr<Cairo::Context> &ctx, Gtk::Allocation &alloc, int db) const;

    std::vector<float> m_level;
    std::vector<float> m_prevlvl;

    // GTK stuff
    Glib::RefPtr<Gdk::Window> m_gdkwindow;
    Glib::Dispatcher m_dispatch_redraw;

private:
    std::vector<float> process_ppm(std::vector<std::vector<float>> &data);
    std::vector<float> process_rms(std::vector<std::vector<float>> &data);
    std::vector<float> process_ebu_short(std::vector<std::vector<float>> &data);
    std::vector<float> process_ebu_momentary(std::vector<std::vector<float>> &data);
    float linear_to_db(float v) const;
    float map_db(float db) const;
    bool m_convert_db;
    size_t m_samplerate;
    float m_dbmin;
    float m_dbmax;
    // Drawing properties
    float m_inmargin;
    bool m_showticks;
    float m_falloff_speed;
    std::chrono::time_point<std::chrono::steady_clock> m_start;
    Monitor *m_monitor;
};
} // namespace Wavr
