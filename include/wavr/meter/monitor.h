#pragma once
#include <algorithm>
#include <vector>

namespace Wavr {
class Monitor {
public:
    typedef std::vector<float> dlevels_t;
    typedef std::vector<std::vector<float>> data_t;

    Monitor() {}
    virtual ~Monitor() {}
    dlevels_t process(const size_t frame_size, const unsigned short channels, const float *data);
    virtual dlevels_t process(const data_t &) {
        dlevels_t lvl(1);
        return lvl;
    }
    virtual bool is_output_linear() { return true; }
};
} // namespace Wavr
